import time
import pandas as p
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import cross_val_score

print('Loading Data')
startTime = time.time()
train_file = p.read_pickle('all_domains_tweets.pkl')
X = train_file.full_clean_text.values
X_crossValidation = train_file.full_clean_text.values
y = train_file.label.values
y_crossValidation = train_file.label.values
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.20)
print('Loaded Data in ' + str(time.time() - startTime) + ' seconds\n')

countVectorizer = CountVectorizer(stop_words='english', lowercase=True)

classifier = MLPClassifier(activation='relu', alpha=0.0001, epsilon=1e-08, hidden_layer_sizes=(20,20), solver='adam', early_stopping=True)
#classifier= LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=1.0, fit_intercept=True, intercept_scaling=1, class_weight=None, random_state=None, solver='lbfgs', max_iter=100, multi_class='multinomial', verbose=0, warm_start=True, n_jobs=1)

pipe = make_pipeline(countVectorizer, classifier)

startTime = time.time()
print('Training Model')
pipe.fit(X_train, y_train)
print('Model Trained in ' + str(time.time() - startTime) + '\n')

startTime = time.time()
print('Testing Model')
vectorizedTestData = countVectorizer.transform(X_test)
predictions = classifier.predict(vectorizedTestData)
print('Testing Completed in ' + str(time.time() - startTime) + '\n')

print('Accuracy')
print(accuracy_score(y_test,predictions))

print('\nConfusion Matrix')
print(confusion_matrix(y_test,predictions))


precision, recall, f1Score, support = (precision_recall_fscore_support(y_test, predictions, average='weighted'))
print('\nPrecision : ' + str(precision))
print('\nRecall : ' + str(recall))
print('\nF-1 Score: ' + str(f1Score))

crossValidationVectorizer = CountVectorizer(stop_words='english', lowercase=True)
X_crossValidation = crossValidationVectorizer.fit_transform(X_crossValidation)
scores = cross_val_score(classifier, X_crossValidation, y_crossValidation, cv=5)
print('\nCross Validation Score : ' + str(scores))
print('\nAverage %0.2f (+/- %0.2f)' % (scores.mean(), scores.std() * 2))