# coding: utf-8


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import pandas as pd

from scipy import sparse

from sklearn.decomposition import LatentDirichletAllocation, NMF
from sklearn.externals import joblib


from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import cross_val_score



n_topics = 4

lda = LatentDirichletAllocation(n_topics=n_topics,
                               random_state=0)

train_file = pd.read_pickle('../all_domains_tweets.pkl')

X = train_file.full_clean_text.values
Y = train_file.label.values

tfVectorizer = TfidfVectorizer(stop_words='english', lowercase=True)

vect_tweets = tfVectorizer.fit_transform(X)



n_top_words = 50

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += " ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        print(message)
    print()
    
feature_names = tfVectorizer.get_feature_names()


n_topics = 4
nmf = NMF(n_components=n_topics, random_state=1, alpha=.1, l1_ratio=.5).fit(vect_tweets)



nmf_kull = NMF(n_components=4, random_state=1,
          beta_loss='kullback-leibler', solver='mu', max_iter=1000, alpha=.1,
          l1_ratio=.5).fit(vect_tweets)


n_top_words = 100
print("\nTopics in NMF model (Frobenius norm):")
tfidf_feature_names = tfVectorizer.get_feature_names()
print_top_words(nmf_kull, tfidf_feature_names, n_top_words)


transformed_tf_nmf_kull = nmf_kull.transform(vect_tweets)
transformed_tf_nmf = nmf.transform(vect_tweets)

tm_nmf_kull_prediction_topics = np.argmax(transformed_tf_nmf_kull, axis=1)
tm_nmf_prediction_topics = np.argmax(transformed_tf_nmf, axis=1)



#nmf transform answer for old data
#{'sport': 17920, 'politics': 11553, 'music': 6597, 'food': 5372}
#LDA analysis for new data
#{'sport': 9224, 'politics': 3308, 'music': 5156, 'food': 1637}

#nmf transform answer for new data
# {'sport': 26994, 'politics': 18239, 'music': 11525, 'food': 9952}
# nmf kull transform answer for new data
#{'sport': 27892, 'politics': 22501, 'music': 18250, 'food': 23578}
#LDA transform answer for new data
# {'sport': 3339, 'politics': 6701, 'music': 6532, 'food': 5580}




mapping = {
    0:'sport',
    1:'politics',
    2:'music',
    3:'food'
}
tm_nmf_kull_prediction_topics[:5]
train_file["topic_modelling_pred_nmf"] = list(map(lambda pred_index: mapping[pred_index], tm_nmf_prediction_topics))
train_file["topic_modelling_pred_nmf_kull"] = list(map(lambda pred_index: mapping[pred_index], tm_nmf_kull_prediction_topics))
valid_data_df =     train_file[(train_file["label"] == train_file["topic_modelling_pred_nmf"]) & (train_file["topic_modelling_pred_nmf"] == train_file["topic_modelling_pred_nmf_kull"])]



valid_data_df["label"].value_counts()



final_valid_data_df = valid_data_df[valid_data_df["label"] == "music"][:9750]
for domain in mapping.values():
    if domain != "music":
        final_valid_data_df = pd.concat([final_valid_data_df, valid_data_df[valid_data_df["label"] == domain][:9750]])

final_valid_data_df = final_valid_data_df.sample(frac=1)



final_valid_data_df.to_pickle("../validated_twitter_data.pkl")

