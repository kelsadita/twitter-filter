import pickle

import numpy as np
import pandas as pd

from nltk.corpus import stopwords

stopwords = set(stopwords.words('english'))


def load_word2vec(filename):
    # Returns a dict containing a {word: numpy array for a dense word vector} mapping.
    # It loads everything into memory.

    w2vec = {}
    with open(filename, "rb") as f_in:
        for line in f_in.readlines():
            line_split = line.decode('utf-8').replace("\n", "").split()
            w = line_split[0]
            vec = np.array([float(x) for x in line_split[1:]])
            w2vec[w] = vec
    return w2vec


def create_train_data(tweets):
    w2vec = load_word2vec('glove.6B.50d.txt')
    clean_tweets = tweets['clean_text'].tolist()
    clean_tweets_label = tweets['label'].tolist()
    print(clean_tweets[:10])
    train_vec = np.zeros((1, 50))
    label_vec = ['dumy']
    for i in range(len(clean_tweets)):
        line = clean_tweets[i]
        avg_vec = np.zeros((1, 50))
        flag = 0
        for word in line.split(" "):
            if word in stopwords:
                continue
            if word in w2vec.keys():
                avg_vec += w2vec[word]
            else:
                flag = 1
                break
        if flag == 0:
            avg_vec = avg_vec / len(line)
            train_vec = np.vstack((train_vec, avg_vec))
            if clean_tweets_label[i] == 'sport':
                label_vec.append(0)
            if clean_tweets_label[i] == 'music':
                label_vec.append(1)
            if clean_tweets_label[i] == 'politics':
                label_vec.append(2)
            if clean_tweets_label[i] == 'food':
                label_vec.append(3)
    train_vec = np.delete(train_vec, 0, 0)
    label_vec = np.delete(label_vec, 0, 0)
    return train_vec, label_vec


tweets = pd.read_pickle("../../validated_twitter_data_heldout.pkl")
# tweets = tweets[tweets.label != 'technology']
print(tweets.head())
X_data, y_labels = create_train_data(tweets)
with open('data_heldout_no_stop.pkl', 'wb') as f:
    pickle.dump(X_data, f)
with open('label_heldout_no_stop.pkl', 'wb') as f:
    pickle.dump(y_labels, f)
