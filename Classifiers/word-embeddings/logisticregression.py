import pickle

import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, precision_recall_fscore_support, accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import label_binarize

with open('data.pkl', 'rb') as f:
    X_data = pickle.load(f)
with open('label.pkl', 'rb') as f1:
    y_labels = pickle.load(f1)

with open('data_heldout.pkl', 'rb') as f:
    X_heldout = pickle.load(f)
with open('label_heldout.pkl', 'rb') as f1:
    y_heldout = pickle.load(f1)

X_train, X_test, y_train, y_test = X_data, X_heldout, y_labels, y_heldout


# train_test_split(X_data, y_labels, test_size=0.3, random_state=101)


def logistic(t_data, t_label, v_data, v_label):
    clf = LogisticRegression(random_state=0, solver='lbfgs', multi_class='multinomial').fit(t_data, t_label)
    label_predict = clf.predict(v_data)

    print('\nClassification Report:')
    print(classification_report(v_label, label_predict))

    print("Overall Score:")
    precision, recall, f1Score, support = (precision_recall_fscore_support(v_label, label_predict, average='weighted'))
    print('Accuracy: ' + str(accuracy_score(v_label, label_predict)))
    print('Precision: ' + str(precision))
    print('Recall: ' + str(recall))
    print('F-1 Score: ' + str(f1Score))


def roc_logistsic(t_data, t_label, v_data, v_label):
    l_list = ['sport', 'music', 'politics', 'food']
    clf = OneVsRestClassifier(LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=1.0,
                                                 fit_intercept=True, intercept_scaling=1, class_weight=None,
                                                 random_state=None,
                                                 solver='lbfgs', max_iter=100, multi_class='multinomial', verbose=0,
                                                 warm_start=True, n_jobs=1))
    t_label = t_label.tolist()
    t_label = np.array([int(x) for x in t_label])
    v_label = v_label.tolist()
    v_label = np.array([int(x) for x in v_label])
    t_label = label_binarize(t_label, classes=[0, 1, 2, 3])
    v_label = label_binarize(v_label, classes=[0, 1, 2, 3])
    y_score = clf.fit(t_data, t_label).decision_function(v_data)

    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(4):
        fpr[i], tpr[i], _ = roc_curve(v_label[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Plot of a ROC curve for a specific class
    for i in range(4):
        # plt.figure()
        plt.plot(fpr[i], tpr[i], label=l_list[i] + ': (area = %0.2f)' % roc_auc[i])
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


logistic(X_train, y_train, X_test, y_test)
roc_logistsic(X_train, y_train, X_test, y_test)
