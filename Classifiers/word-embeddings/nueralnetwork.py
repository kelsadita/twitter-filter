import pickle

import keras
import matplotlib.pyplot as plt
import numpy as np
from keras import metrics
from keras.layers import Dense
from keras.models import Sequential
from sklearn.metrics import classification_report, precision_recall_fscore_support, accuracy_score
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize

with open('data.pkl', 'rb') as f:
    X_data = pickle.load(f)
with open('label.pkl', 'rb') as f1:
    y_labels = pickle.load(f1)

with open('data_heldout_no_stop.pkl', 'rb') as f:
    X_heldout = pickle.load(f)
with open('label_heldout_no_stop.pkl', 'rb') as f1:
    y_heldout = pickle.load(f1)

X_train, X_test, y_train, y_test = X_data, X_heldout, y_labels, y_heldout


# train_test_split(X_data,y_labels,test_size=0.3,random_state=101)


def nn_model(t_data, t_label, v_data, v_label):
    global label_predict_probab
    v_label = [int(label) for label in v_label]
    model = Sequential()
    model.add(Dense(50, activation='relu', input_dim=50))
    model.add(Dense(50, activation='relu'))
    # model.add(Dense(50, activation='relu'))
    # model.add(Dense(50, activation='relu'))
    model.add(Dense(4, activation='softmax'))
    model.compile(optimizer='sgd',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    one_hot_labels = keras.utils.to_categorical(t_label, num_classes=4)
    model.fit(t_data, one_hot_labels, epochs=500, batch_size=128)
    one_hot_validation = keras.utils.to_categorical(v_label, num_classes=4)
    score = model.evaluate(v_data, one_hot_validation, batch_size=128)
    print(score)
    label_predict_probab = model.predict(v_data)
    label_predict = label_predict_probab.argmax(axis=1)

    print('\nClassification Report:')
    print(classification_report(v_label, label_predict))

    print("Overall Score:")
    precision, recall, f1Score, support = (precision_recall_fscore_support(v_label, label_predict, average='weighted'))
    print('Accuracy: ' + str(accuracy_score(v_label, label_predict)))
    print('Precision: ' + str(precision))
    print('Recall: ' + str(recall))
    print('F-1 Score: ' + str(f1Score))


nn_model(X_train, y_train, X_test, y_test)


def roc_nn_model(t_data, t_label, v_data, v_label):
    l_list = ['sport', 'music', 'politics', 'food']
    global label_predict_probab
    y_score = label_predict_probab
    t_label = t_label.tolist()
    t_label = np.array([int(x) for x in t_label])
    v_label = v_label.tolist()
    v_label = np.array([int(x) for x in v_label])
    t_label = label_binarize(t_label, classes=[0, 1, 2, 3])
    v_label = label_binarize(v_label, classes=[0, 1, 2, 3])
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(4):
        fpr[i], tpr[i], _ = roc_curve(v_label[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Plot of a ROC curve for a specific class
    for i in range(4):
        # plt.figure()
        plt.plot(fpr[i], tpr[i], label=l_list[i] + ': (area = %0.2f)' % roc_auc[i])
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


roc_nn_model(X_train, y_train, X_test, y_test)
