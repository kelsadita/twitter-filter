import time
import pandas as p
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.svm import LinearSVC
from sklearn.preprocessing import label_binarize
import matplotlib.pyplot as plt
from sklearn.multiclass import OneVsRestClassifier
import numpy as np
from sklearn.metrics import roc_curve, auc


def roc(t_data, t_label, v_data, v_label):
    clf = OneVsRestClassifier(LinearSVC())
    t_label = np.array([int(x) for x in t_label])
    v_label = np.array([int(x) for x in v_label])
    t_label = label_binarize(t_label, classes=[0, 1, 2, 3])
    v_label = label_binarize(v_label, classes=[0, 1, 2, 3])
    y_score = clf.fit(t_data, t_label).predict(v_data)

    l_list = ['Sports', 'Music', 'Politics', 'Food']
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(4):
        fpr[i], tpr[i], _ = roc_curve(v_label[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Plot of a ROC curve for a specific class
    for i in range(4):
        # plt.figure()
        plt.plot(fpr[i], tpr[i], label=str(l_list[i]) + ' - ROC curve (area = %0.8f)' % roc_auc[i])
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC - LinearSVM - Count Vectorization')
    plt.legend(loc="lower right")
    plt.show()


print('Loading Data')
startTime = time.time()
train_file = p.read_pickle('../../validated_twitter_data.pkl')
heldout_file = p.read_pickle('../../validated_twitter_data_heldout.pkl')
X = train_file.full_clean_text.values
u = set()
for textInp in train_file.label:
    u.add(textInp)
print(u)
X_crossValidation = train_file.full_clean_text.values
y = train_file.label.values
y_crossValidation = train_file.label.values
# X_train, X_test, y_train, y_test = train_test_split(
#     X, y, test_size=0.20)
X_train, y_train = X, y
X_test, y_test = heldout_file.full_clean_text.values, heldout_file.label.values
print('Loaded Data in ' + str(time.time() - startTime) + ' seconds\n')

X_train_roc = X_train
X_test_roc = X_test
y_train_roc = [0 if y == 'politics' else 1 if y == 'sport' else 2 if y == 'music' else 3 for y in y_train]
y_test_roc = [0 if y == 'politics' else 1 if y == 'sport' else 2 if y == 'music' else 3 for y in y_test]

countVectorizer_roc = CountVectorizer(stop_words='english', lowercase=True)
X_train_roc = countVectorizer_roc.fit_transform(X_train_roc)
X_test_roc = countVectorizer_roc.transform(X_test_roc)
countVectorizer = CountVectorizer(stop_words='english', lowercase=True)
countVectorizer = CountVectorizer(stop_words='english', lowercase=True)

classifier = LinearSVC()

pipe = make_pipeline(countVectorizer, classifier)

startTime = time.time()
print('Training Model')
pipe.fit(X_train, y_train)
print('Model Trained in ' + str(time.time() - startTime) + '\n')

startTime = time.time()
print('Testing Model')
vectorizedTestData = countVectorizer.transform(X_test)
predictions = classifier.predict(vectorizedTestData)
print('Testing Completed in ' + str(time.time() - startTime) + '\n')

print('\nClassification Report')
print(classification_report(y_test, predictions))

precision, recall, f1Score, support = (precision_recall_fscore_support(y_test, predictions, average='weighted'))

print('\nAccuracy: ' + str(accuracy_score(y_test, predictions)))
print('\nPrecision : ' + str(precision))
print('\nRecall : ' + str(recall))
print('\nF-1 Score: ' + str(f1Score))

crossValidationVectorizer = CountVectorizer(stop_words='english', lowercase=True)
X_crossValidation = crossValidationVectorizer.fit_transform(X_crossValidation)
scores = cross_val_score(classifier, X_crossValidation, y_crossValidation, cv=5, scoring='f1_macro')
print('\nCross Validation Score : ' + str(scores))
print('\nAverage F1-Macro Score %0.8f (+/- %0.8f)' % (scores.mean(), scores.std() * 2))
roc(X_train_roc, y_train_roc, X_test_roc, y_test_roc)
