# coding: utf-8

# ## Imports

# In[1]:

import pandas as pd
import numpy as np
import os
import json
import re
import emoji
import nltk
import string

from nltk.tokenize import TweetTokenizer

# ## Preprocessing
# * hashtags and user_mentions preprocessing
# * Appending all the domains

# In[2]:

stop_words = set(nltk.corpus.stopwords.words('english'))
tweet_tokenizer = nltk.tokenize.TweetTokenizer(strip_handles=True, reduce_len=True, preserve_case=False)
translator = str.maketrans('', '', string.punctuation)


def remove_emojis(word):
    word_characters = [str for str in word]
    emoji_list = [character for character in word_characters if character in emoji.UNICODE_EMOJI]
    clean_text = ' '.join([str for str in word.split() if not any(filter(lambda x: x in str, emoji_list))])
    return clean_text


def misc_preprocessing(text):
    # Lowering the word
    text = text.lower()

    # Removing emoji icons
    text = remove_emojis(text)

    # Removing link from the text
    text = re.sub(r'https?:\/\/.*[\r\n]*', '', text, flags=re.MULTILINE)

    preprocessed_words = tweet_tokenizer.tokenize(text)

    # Removing stop words
    #     preprocessed_words = filter(lambda word: word not in stop_words, words)
    preprocessed_words = map(lambda word: word.translate(translator), preprocessed_words)
    preprocessed_words = filter(lambda word: word, preprocessed_words)

    return " ".join(preprocessed_words)


def preprocess_hashtags(hashtags):
    result = []
    hashtagcontents = []
    for hashtag in hashtags:
        hashtagtext = misc_preprocessing(hashtag['text'])
        hashtagcontents = re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', hashtagtext)
        result.extend(hashtagcontents)

    return hashtagcontents


def preprocess_mentions(mentions):
    return [misc_preprocessing(mention['name']) for mention in mentions]


# ## Loading data files and doing preprocessing

# In[3]:

all_tweets = pd.DataFrame([])

# data_directory = 'data-new'
# for filename in os.listdir("../" + data_directory):

df_tweets = pd.read_json("../../tweets-heldout.json")

# hashtags preprocessing
df_tweets["hashtags_preproc"] = df_tweets["hashtags"].apply(preprocess_hashtags)

# Mentions preprocessing
df_tweets["mentions_preproc"] = df_tweets["mentions"].apply(preprocess_mentions)

# Misc text processing
df_tweets["clean_text"] = df_tweets["full_text"].apply(misc_preprocessing)

all_tweets = all_tweets.append(df_tweets)

# In[4]:

all_tweets.head()[['full_text', 'clean_text', 'hashtags_preproc', 'mentions_preproc', 'label', 'id']]

# In[5]:

all_tweets.shape

# In[6]:

all_tweets[all_tweets['clean_text'] == ''].shape

# ## Post processing clean text
# * After processing generating clean text there may be cases where it may not contain any text
# * Remving such occurrences

# In[7]:

all_tweets['clean_text'].replace('', np.nan, inplace=True)

# In[8]:

all_tweets.dropna(subset=['clean_text'], inplace=True)

# In[9]:

all_tweets.shape

# In[10]:

all_tweets.head()

# In[11]:

all_tweets['full_clean_text'] = all_tweets['clean_text'] + ' ' + all_tweets['hashtags_preproc'].apply(
    lambda hashtags: " ".join(hashtags)) + ' ' + all_tweets['mentions_preproc'].apply(
    lambda mentions: " ".join(mentions))

# In[12]:

all_tweets.head()

# In[13]:

all_tweets.columns

# ## Ordering columns in an understandable fashion

# In[14]:

new_order = [2, 0, 7, 8, 1, 5, 4, 6, 3]
# new_order = [0, 6, 7, 1, 4, 3, 5, 2]
all_tweets = all_tweets[all_tweets.columns[new_order]]
all_tweets.head()

# In[15]:

all_tweets.to_pickle('./all_domains_tweets_heldout.pkl')

# In[16]:

all_tweets["label"].value_counts()

# In[ ]:



