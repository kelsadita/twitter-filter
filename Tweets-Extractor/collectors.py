import time

import tweepy
import json

CONSUMER_TOKEN = "Vwr83dky7zxe40Oye3EfaLUfw"
CONSUMER_SECRET = "4tMhUwl6ZHI1M4XlDHJMOM2fDkvSCuvrZaCB5OFFIbSTBFK2g6"

ACCESS_TOKEN = "165839170-8MKlGIj7IU7RkKRhiBOnwgKpcBJLlQHSbg1w8Lnn"
ACCESS_SECRET = "vXvNGr29gdujC8eywvNJhej34tgcLsyIDhkQ3mWoJhF0Q"

MAX_TWEETS = 3000


def get_twitter_api():
    # OAuth process, using the keys and tokens
    auth = tweepy.OAuthHandler(CONSUMER_TOKEN, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Creation of the actual interface, using authentication
    api = tweepy.API(auth)
    return api


class_tweet_handle_mapping_held_out = {
    "sport": [
        # "darrenrovell",
        # "SkySportsNews",
        "Buster_ESPN"
    ],
    "music": [
        "BBC6Music"
    ],
    "food": [
        "NatGeoFood"
    ],
    "politics": [
        "PolitiFact",
    ],
}

class_tweet_handle_mapping = {
    "sport": [
        "espn",
        "olympicchannel",
        "premierleague",
        "nba",
        "FOXSoccer",
        "Wimbledon",
        "TwitterSports",
        "SportsCenter",
        "BBCSport",
        "SInow"
    ],
    "music": [
        "TwitterMusic",
        "pitchfork",
        "bulletmusicnet",
        "RollingStone",
        "billboard",
        "AltPress",
        "SpinninRecords",
        "stereogum",
        "NME",
        "SPIN"
    ],
    "food": [
        "foodandwine",
        "bonappetit",
        "nytfood",
        "epicurious",
        "FoodNetwork",
        "CookingChannel",
        "foodista",
        "tastykitchen",
        "Food52",
        "thekitchn"
    ],
    "politics": [
        "nprpolitics",
        "BBCPolitics",
        "CNNPolitics",
        "nytpolitics",
        "ABCPolitics",
        "NBCPolitics",
        "CBSPolitics",
        "politico",
        "washingtonpost",
        "TotalPolitics"
    ]
}

if __name__ == '__main__':
    api = get_twitter_api()

    data_directory = 'data-heldout'

    for domain, tweet_handles in class_tweet_handle_mapping_held_out.iteritems():
        print("-> Started collecting :: " +  domain)
        tweets_json = []
        for tweet_handle in tweet_handles:
            print("==>" + tweet_handle)
            tweet_count = 0
            while tweet_count != MAX_TWEETS:
                try:
                    for tweet in tweepy.Cursor(api.user_timeline, screen_name='@' + tweet_handle).items():
                        tweet_dict = tweet._json
                        if u'media' in tweet_dict:
                            del tweet_dict[u'media']
                        if u'user' in tweet_dict:
                            del tweet_dict[u'user']
                        if u'retweeted_status' in tweet_dict:
                            del tweet_dict[u'retweeted_status']

                        tweet_dict["label"] = domain
                        tweets_json.append(tweet_dict)

                        if tweet_count == MAX_TWEETS:
                            break
                        tweet_count += 1

                except tweepy.TweepError:
                    print("::Sleeping for 15 minutes")
                    time.sleep(60 * 15)
                    continue
                except StopIteration:
                    break

        with open("./" + data_directory + "/" + domain + ".json", "w") as sports_tweets_file:
            # print "Storing tweets", len(tweets_json)
            sports_tweets_file.write(json.dumps(tweets_json, indent=2))
