import os
import json

if __name__ == '__main__':
    tweets = []
    tweet_data_directory = 'data-heldout'
    for filename in os.listdir(os.path.join(os.getcwd(), "./Tweets-Extractor/" + tweet_data_directory)):
        file_path = os.path.join(os.getcwd(), "./Tweets-Extractor/" + tweet_data_directory + "/")
        with open(file_path + filename) as tweets_file:
            tweets_json = json.load(tweets_file)
            for tweet in tweets_json:
                tweet_details = {}
                tweet_entities = tweet["entities"]
                tweet_details["hashtags"] = tweet_entities["hashtags"]
                tweet_details["mentions"] = tweet_entities["user_mentions"]
                tweet_details["full_text"] = tweet["text"]
                tweet_details["label"] = tweet["label"]
                tweet_details["id"] = tweet["id"]
                tweets.append(tweet_details)

    with open("./tweets-heldout.json", "w") as preprocessed_tweet_file:
        preprocessed_tweet_file.write(json.dumps(tweets, indent=2))
