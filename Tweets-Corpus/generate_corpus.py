import json
import pandas as pd

final_corpus = {
    "description": "We observed that the current tweet based datasets are built for tasks like sentiment analysis but "
                   "not for topic classification. We will attempt to build a data set which comprises of Tweets with "
                   "Category labels of Sports, Music, Food, Politics, Technology...(We will finalize these categories "
                   "after getting a better idea of the retrieved data). With this Classifier, we are planning to "
                   "build a feed filtering system which can cater to the user's field of interest. (Currently, "
                   "a user can search for categories but get tweets from sources they may not be interested in.) Our "
                   "system will help users apply topic filter on their own tweeter feeds.\nThe source of the data is "
                   "Twitter API. To label this data, we will be considering multiple factors. We plan to retrieve the "
                   "data by handles and we can associate a label bias based on the handle (ex: Masterchef America for "
                   "food). Further, we will be referring the topic models created using the 20 newsgroups text "
                   "dataset.\nWe are planning to retrieve and label more than 10,000 tweets. Any Twitter REST API is "
                   "a rate limited for queries in a short burst of time. Over the duration of the project, "
                   "we are sure to retrieve the given amount of tweets. The API provides this data in JSON format "
                   "from which tweet contents can be easily extracted. We will then remove hashtags, handles, links, "
                   "images and emotion characters from each tweet. ",

    "authors": {
        "author1": "Kalpesh Adhatrao",
        "author2": "Akshay Velankar",
        "author3": "Malhar Kulkarni",
        "author4": "Raj Thaker"
    },
    "emails": {
        "email1": "adhatrao@usc.edu",
        "email2": "velankar@usc.edu",
        "email3": "mskulkar@usc.edu",
        "email4": "rpthaker@usc.edu"
    }
}

tweets_df = pd.read_pickle("../validated_twitter_data.pkl")

all_tweets = []
for idx, row in tweets_df.iterrows():
    tweet = {
        "id": row["id"],
        "data": row["full_text"],
        "clean_data": row["full_clean_text"],
        "label": row["label"]
    }
    all_tweets.append(tweet)

final_corpus["corpus"] = all_tweets

with open("final.json", "w") as final_output:
    final_output.write(json.dumps(final_corpus, indent=2))


